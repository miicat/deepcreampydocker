# DeepCreamPyDocker
*Decensoring Hentai with Deep Neural Networks in Docker. Formerly named DeepMindBreak.*
Shortly this is [DeepCreamPy](https://github.com/deeppomf/DeepCreamPy) in Docker

A deep learning-based tool to automatically replace censored artwork in hentai with plausible reconstructions.

The user colors cencored regions green in an image editing program like GIMP or Photoshop. A neural network fills in the censored regions.

DeepCreamPyDocker runs in Docker container, so you don't have to install anything in you computer (except Docker). DeepCreamPyDocker works where Docker works, on Windows, Mac, and Linux. I have tested it on Ubuntu(Linux)

![Censored, decensored](/readme_images/mermaid_collage.png)

## Lastest
Lastest is v1.5.1

## Features
- Decensoring images of ANY size
- Decensoring of ANY shaped censor (e.g. black lines, pink hearts, etc.)
- Higher quality decensors
- Support for mosaic decensors (WIP)
- User interface (WIP)

## Limitations
The decensorship is for color hentai images that have minor to moderate censorship of the penis or vagina. If a vagina or penis is completely censored out, decensoring will be ineffective.

It does NOT work with:
- Black and white/Monochrome image
- Hentai with screentones (e.g. printed hentai)
- Real life porn
- Censorship of nipples
- Censorship of anus
- Animated gifs/videos

## Requirements
- Docker CE
- Python3.6

## Table of Contents
Setup:
* [Running docker yourself](INSTALLATION.md)

Usage:
* [Decensoring tutorial](USAGE.md)
* [Troubleshooting for poor quality decensors](TROUBLESHOOTING.md)

Miscellaneous:
* [FAQ](FAQ.md)



Contributions are welcome! Special thanks to deeppomf, IAmTheRedSpy, 0xb8, deniszh, Smethan, mrmajik45, harjitmoe, itsVale, StartleStars, and SoftArmpit!

## License
This project is licensed under GNU Affero General Public License v3.0.

See [LICENSE.txt](LICENSE.txt) for more information about the license.

## Acknowledgements
Example mermaid image by Shurajo & AVALANCHE Game Studio under [CC BY 3.0 License](https://creativecommons.org/licenses/by/3.0/). The example image is modified from the original, which can be found [here](https://opengameart.org/content/mermaid).

Neural network code is modified from MathiasGruber's project [Partial Convolutions for Image Inpainting using Keras](https://github.com/MathiasGruber/PConv-Keras), which is an unofficial implementation of the paper [Image Inpainting for Irregular Holes Using Partial Convolutions](https://arxiv.org/abs/1804.07723). [Partial Convolutions for Image Inpainting using Keras](https://github.com/MathiasGruber/PConv-Keras) is licensed under the MIT license.

User interface code is modified from Packt's project [Tkinter GUI Application Development Blueprints - Second Edition](https://github.com/PacktPublishing/Tkinter-GUI-Application-Development-Blueprints-Second-Edition). [Tkinter GUI Application Development Blueprints - Second Edition](https://github.com/PacktPublishing/Tkinter-GUI-Application-Development-Blueprints-Second-Edition) is licensed under the MIT license.

Data is modified from gwern's project [Danbooru2017: A Large-Scale Crowdsourced and Tagged Anime Illustration Dataset](https://www.gwern.net/Danbooru2017).

See [ACKNOWLEDGEMENTS.md](ACKNOWLEDGEMENTS.md) for full license text of these projects.

## Known bugs
- 

## Donating
If you like my work, you can donate to me via Paypal: [![Donate](https://img.shields.io/badge/Donate-PayPal-green.svg)](https://www.paypal.me/miicat)
