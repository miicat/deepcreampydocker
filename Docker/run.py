#!/usr/bin/env python3.6
# -*- coding: cp1252 -*-

import os

clear = lambda: os.system('clear')
version = '1.5'
dockerFolder = 'cd dockerfiles && '

# Print main menu
clear()
print ('Do you want to:\n',
        '(B)uild\n',
        '(R)un')
choose = input(': ')

if choose in ('b', 'B'):
    while (True):
        clear()
        # Print list of build options
        print ('Do you want to build:\n',
                '(M)anual build\n',
                '(A)utomated build (when runned, it automatically decensors and exits)')
        chooseBuildMethod = input(': ')

        if chooseBuildMethod in ('m', 'M'):
            print ('Building...')
            # Change bashrc0 to bashrc
            os.system(dockerFolder + 'mv bashrc0 bashrc')
            # Build
            os.system(dockerFolder + 'docker build -t deeppyr-' + version  +  ' .')
            # Change bashrc back normal 
            os.system(dockerFolder + 'mv bashrc bashrc0')
            break
        elif chooseBuildMethod in ('a', 'A'):
            print ('Building...')
            # Change bashrc0 to bashrc
            os.system(dockerFolder + 'mv bashrc1 bashrc')
            # Build
            os.system(dockerFolder + 'docker build -t deeppyr-' + version  +  ' .')
            # Change bashrc back normal 
            os.system(dockerFolder + 'mv bashrc bashrc1')
            break

elif choose in ('r', 'R'):
        clear()
        print ('Running...')
        os.system('docker run -u $(id -u):$(id -g) -i -t --rm --mount source="$(pwd)/input",destination=/home/user/DeepCreamPy/decensor_input,type=bind --mount source="$(pwd)/output",destination=/home/user/DeepCreamPy/decensor_output,type=bind --mount source="$(pwd)/original",destination=/home/user/DeepCreamPy/decensor_input_original,type=bind deeppyr-' + version  + ' bash')
