# Installation

**Lastest release is r-1.5**

## Install Docker to your machine
Follow [these](https://docs.docker.com/install/) instructions

## Install DeepCreamPyDocker to your machine
### Clone repository
Clone this repo and run: git checkout v-(version)

To select release 1.5, run: git checkout v1.5

### Run
To build/run the image, run:

```
$ run.py
```

and select 'Build' and then 'Manual' or 'Automated'. To run the image, choose 'Run'

## How to use DeepCreamPyDocker?
See [this](USAGE.md)
